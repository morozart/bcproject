import torch
import torch.nn as nn
import torch.utils.data


def Conv_Block1():
    layers = nn.Sequential(nn.Conv2d(in_channels = 3, out_channels = 32, kernel_size=(3,3), padding=1),
                           nn.BatchNorm2d(32),
                           nn.ReLU(),
                           nn.MaxPool2d(2)
                           )
    return layers

def Conv_Block2():
    layers = nn.Sequential(nn.Conv2d(in_channels = 32, out_channels = 64, kernel_size=(3,3), padding=1),
                           nn.BatchNorm2d(64),
                           nn.ReLU(),
                           nn.MaxPool2d(2),
                           )
    return layers

def Conv_Block3():
    layers = nn.Sequential(nn.Conv2d(in_channels = 64, out_channels = 128, kernel_size=(3,3), padding=1),
                           nn.BatchNorm2d(128),
                           nn.ReLU(),
                           nn.MaxPool2d(2),
                           )
    return layers
def Conv_Block4():
    layers = nn.Sequential(nn.Conv2d(in_channels = 128, out_channels = 256, kernel_size=(3,3), padding=1),
                           nn.BatchNorm2d(256),
                           nn.ReLU(),
                           nn.MaxPool2d(2),
                           )
    return layers
def Conv_Block5():
    layers = nn.Sequential(nn.Conv2d(in_channels = 256, out_channels = 256, kernel_size=(3,3), padding=1),
                           nn.BatchNorm2d(256),
                           nn.ReLU(),
                           nn.MaxPool2d(2),
                           )
    return layers
def Conv_Block6():
    layers = nn.Sequential(nn.Conv2d(in_channels = 256, out_channels = 512, kernel_size=(3,3), padding=1),
                           nn.BatchNorm2d(512),
                           nn.ReLU(),
                           nn.MaxPool2d(2),
                           )
    return layers
def Conv_Block7():
    layers = nn.Sequential(nn.Conv2d(in_channels = 512, out_channels = 512, kernel_size=(3,3), padding=1),
                           nn.BatchNorm2d(512),
                           nn.ReLU(),
                           nn.MaxPool2d(2),
                           )
    return layers


class Model(torch.nn.Module):
    def __init__(self, nbr_classes=10):
        super().__init__()

        self.conv_block1 = Conv_Block1()
        self.conv_block2 = Conv_Block2()

        self.conv3 = Conv_Block3()
        self.conv4 = Conv_Block4()
        self.conv5 = Conv_Block5()
        self.conv6 = Conv_Block6()
        self.conv7 = Conv_Block7()


        self.lin1 = nn.Linear(512 , nbr_classes)


        self.weight_init()

    def forward(self, x):

        x = x.view(len(x), 3, 128, 128)/255

        x = self.conv_block1(x)
        x = self.conv_block2(x)
        x = self.conv3(x)
        x = self.conv4(x)
        x = self.conv5(x)
        x = self.conv6(x)
        x = self.conv7(x)
        #print(x.shape)
        x = self.lin1(x.view(len(x), 512))

        x = torch.softmax(x, dim=1)

        return x

    def weight_init(self):
        for lay in self.modules():
            if type(lay) in [torch.nn.Conv2d, torch.nn.Linear]:
                torch.nn.init.xavier_uniform_(lay.weight)