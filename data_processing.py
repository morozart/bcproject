import glob
import cv2
import numpy as np
import tensorflow as tf
#"/Users/artemmoroz/Desktop/python_projects/Semestral_Project/{string}/*.png"
#/datagrid/Medical/ArteryPlaque/in_vivo_platy_segmentace/masks_7_11/{string}/Trans/*.png
#/Users/artemmoroz/Desktop/python_projects/Semestral_Project/trans_{string}/*.png
TO_RESIZE = False

def read_files(string):
    image_array = []
    f_name = []
    for img in glob.glob(f"/Users/artemmoroz/Desktop/python_projects/Semestral_Project/trans_{string}/*.png"):
        f_name.append(img[-27:])
        cv_img = cv2.imread(img)
        image_array.append(cv_img)


    nparray = np.array(image_array)

    if string == "progressive":
        labels_array = np.ones(len(nparray))
    else:
        labels_array = np.zeros(len(nparray))

    return nparray, labels_array, f_name, len(nparray)

@tf.function
def scale_resize_image(image):
    image = tf.image.convert_image_dtype(image, tf.float32) # equivalent to dividing image pixels by 255
    image = tf.image.resize(image, (256, 256)) # Resizing the image to 224x224 dimention
    return image

def DataDict():
    prog_array, prog_label, f_name_prog, prog_num = read_files("progressive")
    stab_array, stab_label, f_name_stab, stab_num = read_files("stable")

    data_array = np.concatenate((prog_array, stab_array), axis = 0)

    labels = np.append(prog_label, stab_label)
    if TO_RESIZE:
        for i in range(len(labels)):
            data_array[i] = scale_resize_image(data_array[i])
    f_name = np.append(f_name_prog, f_name_stab)

    assert len(data_array) == len(labels) and len(data_array) == len(f_name) and len(f_name) == len(labels), "Sizes aren't consistent"

    return {"data": data_array, "labels": labels, "file_names": f_name}, (prog_num, stab_num)







