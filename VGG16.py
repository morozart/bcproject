import torch.nn as nn
import torch


def Conv_Block(in_ch, out_ch, kernel, padding):
    layers = nn.Sequential(nn.Conv2d(in_channels = in_ch, out_channels = out_ch, kernel_size=kernel, padding=padding),
                           nn.BatchNorm2d(out_ch),
                           nn.ReLU(),
                           )
    return layers


class Model_VGG(torch.nn.Module):

    def __init__(self, nbr_classes=2):
        super().__init__()

        self.conv_block1 = Conv_Block(in_ch=3, out_ch=64, kernel=(3,3), padding=2)
        self.conv_block2 = Conv_Block(in_ch=64, out_ch=64, kernel=(3,3), padding=1)
        self.MaxPool = nn.MaxPool2d(kernel_size=2, stride=2, padding=0)

        self.conv_block3 = Conv_Block(in_ch=64, out_ch=128, kernel=(3,3), padding=2)
        self.conv_block4 = Conv_Block(in_ch=128, out_ch=128, kernel=(3,3), padding=1)

        self.conv_block5 = Conv_Block(in_ch=128, out_ch=256, kernel=(3,3), padding=2)
        self.conv_block6 = Conv_Block(in_ch=256, out_ch=256, kernel=(3,3), padding=1)

        self.conv_block7 = Conv_Block(in_ch=256, out_ch=512, kernel=(3,3), padding=2)
        self.conv_block8 = Conv_Block(in_ch=512, out_ch=512, kernel=(3,3), padding=2)
        self.conv_block9 = Conv_Block(in_ch=512, out_ch=512, kernel=(3,3), padding=1)

        self.conv_block10 = Conv_Block(in_ch=512, out_ch=512, kernel=(3,3), padding=2)
        self.conv_block11 = Conv_Block(in_ch=512, out_ch=512, kernel=(3,3), padding=2)
        self.conv_block12 = Conv_Block(in_ch=512, out_ch=512, kernel=(3,3), padding=1)
        self.relu = nn.ReLU()
        self.lin1 = nn.Linear(512 * 11 * 11 , 4096, bias=True)
        self.lin2 = nn.Linear(4096 , 4096, bias=True)
        self.lin3 = nn.Linear(4096, nbr_classes, bias=True)

        self.weight_init()

    def forward(self, x):

        x = x/255 * 2 - 1

        x = x.permute(0,3,1,2)

        x = self.conv_block1(x)
        x = self.conv_block2(x)
        x = self.MaxPool(x)

        x = self.conv_block3(x)
        x = self.conv_block4(x)
        x = self.MaxPool(x)

        x = self.conv_block5(x)
        x = self.conv_block6(x)
        x = self.MaxPool(x)

        x = self.conv_block7(x)
        x = self.conv_block8(x)
        x = self.conv_block9(x)
        x = self.MaxPool(x)

        x = self.conv_block10(x)
        x = self.conv_block11(x)
        x = self.conv_block12(x)
        x = self.MaxPool(x)
        #print(x.shape)
        x = self.lin1(x.reshape(x.shape[0], 512 * 11 * 11))
        x = self.relu(x)
        x = self.lin2(x.reshape(x.shape[0], 4096))
        x = self.relu(x)
        #print(x.shape)
        x = self.lin3(x.reshape(x.shape[0], 4096))

        x = torch.softmax(x, dim=1)
        #print("SOFTMAX VALUES:", x[0])
        return x

    def weight_init(self):
        for lay in self.modules():
            if type(lay) in [torch.nn.Conv2d, torch.nn.Linear]:
                torch.nn.init.xavier_uniform_(lay.weight)
