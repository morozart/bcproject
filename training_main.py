import torch
import Datasets
from Datasets import LoadDataset
from Alex_net import ALEX
from time import gmtime, strftime

data_dict = Datasets.split_data_to_datasets()

train_set = LoadDataset(data_dict["train"]["data"], data_dict["train"]["labels"])

training_generator = torch.utils.data.DataLoader(train_set, batch_size = 12, shuffle = True, num_workers = 0)


val_set = LoadDataset(data_dict["val"]["data"], data_dict["val"]["labels"])
validation_generator = torch.utils.data.DataLoader(val_set, batch_size = 12, shuffle = False, num_workers = 0)


test_set = LoadDataset(data_dict["test"]["data"], data_dict["test"]["labels"])
test_generator = torch.utils.data.DataLoader(test_set, batch_size = 150, shuffle = False, num_workers = 0)


def confusion_matrix(prediction, label, num_classes=2):
    nb_classes = 2

    confusion_matrix = torch.zeros(nb_classes, nb_classes)
    with torch.no_grad():

        preds = torch.max(prediction, 1)
        preds = preds.values
        #print("PREDICTIONS", preds)
        for i in range(len(preds)):
            if preds[i] == 1 and preds[i] == label[i]:
                confusion_matrix[0][0] += 1
            elif preds[i] == 0 and preds[i] == label[i]:
                confusion_matrix[1][1] += 1
            elif preds[i] == 1 and label[i] == 0:
                confusion_matrix[0][1] += 1
            elif preds[i] == 0 and label[i] == 1:
                confusion_matrix[1][0] += 1
    print("Confusion:", confusion_matrix)
    return


def one_epoch(batch,  loss_fnc, model, optimizer=None, test = False):
    device = torch#.device(1)
    batch_accuracy = list()
    model = model#.to(device)
    if optimizer is not None:
        model.train()
    else:
        model.eval()
    running_loss = 0
    acc_list = []
    print("Alex SGD0")
    for index, batch in enumerate(batch):

        data = batch["data"]#.to(device)
        labels = batch["labels"]#.to(device)
        drop = True

        if optimizer is None:
            drop = False

        pred = model.forward(data, drop=drop)
        #print(pred)
        loss = loss_fnc(pred, labels)

        if test:
            confusion_matrix(pred, labels)

        if optimizer is not None:

            loss.backward()
            optimizer.step()
            optimizer.zero_grad()

        running_loss += loss.item()

        acc_list.append(torch.argmax(pred, dim=1) == labels)


    overall_accuracy = torch.cat(acc_list).float().mean() * 100
    print("BATCH_size", len(batch["data"]))
    return overall_accuracy, running_loss / len(batch)


model = ALEX()

trn_results = []
val_results = []
trn_loss_list = []
val_loss_list = []

optimizer = torch.optim.SGD(model.parameters(), lr=0.065, weight_decay=0.01)
criterion = torch.nn.CrossEntropyLoss()

for e in range(150):
    ''' Training Epoch '''
    trn_acc, trn_loss = one_epoch(batch=training_generator, model=model, optimizer=optimizer, loss_fnc=criterion)
    trn_results.append(trn_acc.item()), trn_loss_list.append(trn_loss)

    print(f'Epoch: {e} \t Trn Acc: {trn_acc :.3f} \t Trn Loss: {trn_loss :.3f}\n', end='\t')

    val_acc, val_loss = one_epoch(batch=validation_generator, model=model, optimizer=None, loss_fnc=criterion)
    val_results.append(val_acc.item()), val_loss_list.append(val_loss)
    print(f'Val Acc: {val_acc :.3f} \t Val Loss: {val_loss :.3f}\n')
    print(val_acc.item())

print(trn_results, "TRN_accuracy")
print(val_results, "VAL_accuracy")
print(trn_loss_list, "TRN_loss")
print(val_loss_list, "VAL_loss")

test_acc, test_loss = one_epoch(batch=test_generator, model=model, optimizer=None, loss_fnc=criterion, test = True)

print(test_acc, "TEST acc")
print(test_loss, "TEST loss")

