import torch.nn as nn
import torch


def Conv_Block(in_ch, out_ch, kernel, padding, stride = 1):
    layers = nn.Sequential(nn.Conv2d(in_channels = in_ch, out_channels = out_ch, kernel_size=kernel, padding=padding, stride=stride),
                           nn.BatchNorm2d(out_ch),
                           nn.LeakyReLU(0.1),
                           )
    return layers


class ALEX(torch.nn.Module):

    def __init__(self, nbr_classes=2):
        super().__init__()

        self.conv_block1 = Conv_Block(in_ch=3, out_ch=96, kernel=(11,11), padding=2, stride=4)
        self.conv_block2 = Conv_Block(in_ch=96, out_ch=256, kernel=(3,3), padding=2)
        self.MaxPool = nn.MaxPool2d(kernel_size=3, stride=2, padding=0)

        self.conv_block3 = Conv_Block(in_ch=256, out_ch=384, kernel=(3,3), padding=1)
        self.conv_block4 = Conv_Block(in_ch=384, out_ch=384, kernel=(3,3), padding=1)

        self.conv_block5 = Conv_Block(in_ch=384, out_ch=256, kernel=(3,3), padding=1)
        self.conv_block6 = Conv_Block(in_ch=256, out_ch=256, kernel=(3,3), padding=1)

        self.lin1 = nn.Linear(256 * 7 * 7, 4096)
        self.lin2 = nn.Linear(4096, nbr_classes)
        self.dropout20 = nn.Dropout(p=0.20)
        self.dropout35 = nn.Dropout(p=0.35)
        self.leaky_r = nn.LeakyReLU(0.1)
        self.weight_init()

    def forward(self, x, drop):

        x = x/255

        x = x.permute(0,3,1,2)

        x = self.conv_block1(x)
        x = self.dropout20(x)
        x = self.MaxPool(x)

        x = self.conv_block2(x)
        x = self.dropout35(x)
        x = self.MaxPool(x)

        x = self.conv_block3(x)
        x = self.dropout35(x)
        x = self.conv_block4(x)
        x = self.dropout35(x)
        x = self.conv_block5(x)
        x = self.dropout35(x)
        x = self.MaxPool(x)

        #print("first full connect", x.shape)
        x = self.lin1(x.reshape(x.shape[0], 256 * 7 * 7))
        x = self.dropout20(x)
        x = self.leaky_r(x)
       # print("second_full_connect", x.shape)
        x = self.lin2(x.reshape(x.shape[0], 4096))
       # print("softmax", x.shape)
        x = torch.softmax(x, dim=1)

        return x

    def weight_init(self):
        for lay in self.modules():
            if type(lay) in [torch.nn.Conv2d, torch.nn.Linear]:
                torch.nn.init.xavier_uniform_(lay.weight)
