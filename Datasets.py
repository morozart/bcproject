import numpy as np
import data_processing
import random
import torch.utils


def split_data_to_datasets():

    data_dict, _ = data_processing.DataDict()
    available_index = np.array(range(len(data_dict["data"])))

    train_rand_index = random.sample(range(0, len(available_index)), round(0.7 * len(data_dict["data"])))

    train_data = {"data": data_dict["data"][train_rand_index], "labels": data_dict["labels"][train_rand_index], "file_names": data_dict["file_names"][train_rand_index]}

    available_index = np.delete(available_index, train_rand_index)
    random.shuffle(available_index)

    val_rand_index = available_index[:int(len(available_index)/2)]
    test_rand_index = available_index[int(len(available_index)/2):]

    val_data = {"data": data_dict["data"][val_rand_index], "labels": data_dict["labels"][val_rand_index], "file_names": data_dict["file_names"][val_rand_index]}
    test_data = {"data": data_dict["data"][test_rand_index], "labels": data_dict["labels"][test_rand_index], "file_names": data_dict["file_names"][test_rand_index]}

    for index in range(len(train_data["data"])):

        train_data["data"][index] = torch.from_numpy(train_data["data"][index])

    for index in range(len(val_data["data"])):

        val_data["data"][index] = torch.from_numpy(val_data["data"][index])

    for index in range(len(test_data["data"])):

        test_data["data"][index] = torch.from_numpy(test_data["data"][index])

    return {"train": train_data, "val": val_data, "test": test_data}


data_dict = split_data_to_datasets()


class LoadDataset(torch.utils.data.Dataset):
    def __init__(self, data, labels, augment = False):
        'Initialization'
        self.labels = labels
        self.data = data

    def __len__(self):
        'Denotes the total number of samples'
        return len(self.data)

    def __getitem__(self, index):
        'Generates one sample of data'

        batch = {"data": torch.tensor(self.data[index], dtype=torch.float), 'labels': torch.tensor(self.labels[index], dtype=torch.long)}
        return batch

